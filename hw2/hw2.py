#!/usr/bin/python3.6
import sys
sys.path.append('/home/mattscar/Homework/coe332-mas9263/hw1')
from hw1 import *

id      = 'id'
time    = 'year'
value   = 'sunspots'

#=============== 1 ====================

# a.
def test_read_from_csv_return():
    assert type(read_from_csv()) == list

# b.
def test_read_from_csv_elements():
    assert [type(dict_i) == dict for dict_i in read_from_csv()]

# c.
def test_read_from_csv_keys():
    assert [len(dict_i.keys()) == 3 for dict_i in read_from_csv()]

# d.
def test_read_from_csv_value_types():
    assert [type(dict_i[id]) == int for dict_i in read_from_csv()]
    assert [type(dict_i[time]) == int for dict_i in read_from_csv()]
    assert [type(dict_i[value]) == int for dict_i in read_from_csv()]

# e.
def test_read_from_csv_num_dicts():
    assert len(read_from_csv()) == 100

# f.
def test_read_from_csv_values():
    assert read_from_csv()[0][id] == 0
    assert read_from_csv()[0][time] == 1770
    assert read_from_csv()[0][value] == 101
    assert read_from_csv()[-1][id] == 99
    assert read_from_csv()[-1][time] == 1869
    assert read_from_csv()[-1][value] == 74


#=============== 2 =======================

# a.
def test_read_on_timespan_return():
    assert type(read_on_timespan()) == list

# b.
def test_read_on_timespan_elements():
    assert [type(dict_i) == dict for dict_i in read_on_timespan()]

# c.
def test_read_on_timespan_keys():
    assert [len(dict_i.keys()) == 3 for dict_i in read_on_timespan()]

# d. 
def test_read_on_timespan_value_types():
    assert [type(dict_i[id]) == int for dict_i in read_on_timespan()]
    assert [type(dict_i[time]) == int for dict_i in read_on_timespan()]
    assert [type(dict_i[value]) == int for dict_i in read_on_timespan()]

# e.
def test_read_on_timespan_unspecified_num_dicts():
    assert len(read_on_timespan()) == 100

# f.
def test_read_on_timespan_start_result():
    assert len(read_on_timespan(start=1800)) == 70
    assert read_on_timespan(start=1800)[0][time] == 1800
    assert read_on_timespan(start=1800)[-1][time] == 1869

# g.
def test_read_on_timespan_end_result():
    assert len(read_on_timespan(end=1800)) == 31
    assert read_on_timespan(end=1800)[0][time] == 1770
    assert read_on_timespan(end=1800)[-1][time] == 1800

# h.
def test_read_on_timespan_start_end_result():
    assert len(read_on_timespan(start=1800, end=1820)) == 21
    assert read_on_timespan(start=1800, end=1820)[0][time] == 1800
    assert read_on_timespan(start=1800, end=1820)[-1][time] == 1820

# i.
def test_read_on_timespan_irregularities():
    assert len(read_on_timespan(start=1800, end=1799)) == 0
    # add?

#=================== 3 =======================

# a.
def test_read_with_limit_offset_return():
    assert type(read_with_limit_offset()) == list

# b.
def test_read_with_limit_offset_elements():
    assert [type(dict_i) == dict for dict_i in read_with_limit_offset()]

# c.
def test_read_with_limit_offset_keys():
    assert [len(dict_i.keys()) == 3 for dict_i in read_with_limit_offset()]

# d. 
def test_read_with_limit_offset_value_types():
    assert [type(dict_i[id]) == int for dict_i in read_with_limit_offset()]
    assert [type(dict_i[time]) == int for dict_i in read_with_limit_offset()]
    assert [type(dict_i[value]) == int for dict_i in read_with_limit_offset()]

# e.
def test_read_with_limit_offset_unspecified_num_dicts():
    assert len(read_with_limit_offset()) == 100

# f.
def test_read_with_limit_offset_limit_result():
    for LIMIT in range(1,100):
        assert len(read_with_limit_offset(limit=LIMIT)) == LIMIT
        assert read_with_limit_offset(limit=LIMIT)[0][time] == 1770
        assert read_with_limit_offset(limit=LIMIT)[-1][time] == 1769+LIMIT

# g.
def test_read_with_limit_offset_offset_result():
    for OFFSET in range(1,100):
        assert len(read_with_limit_offset(offset=OFFSET)) == 100-OFFSET
        assert read_with_limit_offset(offset=OFFSET)[0][time] == 1770+OFFSET
        assert read_with_limit_offset(offset=OFFSET)[-1][time] == 1869

# h.
def test_read_with_limit_offset_limit_offset_result():
    for OFFSET in range(1, 100): # slow but thorough
        for LIMIT in range(OFFSET, 100-OFFSET):
            assert len(read_with_limit_offset(limit=LIMIT, offset=OFFSET)) == LIMIT or len(read_with_limit_offset(limit=LIMIT, offset=OFFSET)) == 100-OFFSET
            assert read_with_limit_offset(limit=LIMIT, offset=OFFSET)[0][time] == 1770+OFFSET
            assert read_with_limit_offset(limit=LIMIT, offset=OFFSET)[-1][time] == 1769+OFFSET+LIMIT or read_with_limit_offset(limit=LIMIT, offset=OFFSET) == 1869

# i.
def test_read_with_limit_offset_irregularities():
    assert len(read_with_limit_offset(limit=101)) == 100
    assert len(read_with_limit_offset(offset=101)) == 0
    assert len(read_with_limit_offset(limit=0)) == 0
    # add?
