import uuid
import redis
import json
import time

rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)

def generate_uuid():
    return uuid.uuid4()

# other helpers

class Job(object):
    # instantiate job
    def __init__(self, jid='', status='', start=0, end=0, limit=0, offset=0):
        self.job_dict = {}
        if jid:
            self.jid = jid
            self.job_dict['jid'] = jid
        else:
            self.jid = self.generate_jid()
            self.job_dict['jid'] = self.jid
        if status:
            self.status = status
            self.job_dict['status'] = status
        if start:
            self.start = start
            self.job_dict['start'] = start
        if end:
            self.end = end
            self.job_dict['end'] = end
        if limit:
            self.limit = limit
            self.job_dict['limit'] = limit
        if offset:
            self.offset = offset
            self.job_dict['offset'] = offset
        t = time.ctime()
        self.create_time = t
        self.last_update_time = t

    def generate_jid(self):
        return str(uuid.uuid4())

    def generate_job_key(self, jid):
        return 'job.{}'.format(jid)

    def get_job_dict(self):
        return self.job_dict

    def add_job(self, jid, status, start, end):
        rd.set(self.generate_job_key(jid), self.job_dict)
        return

    # Problem 2
    def store_job(self):
        rd.set(self.jid, json.dumps(self.job_dict))
        return self.job_dict

# Problem 1
#def instantiate_job(jid, status, start, end):
#    ''' Create a Python object representing a job. '''
    #job = Job
    #job object

# Problem 3
def retrieve_job(uuid):
    if rd.get(uuid):
        return 'job.' + str(rd.get(uuid))
    else:
        return 0

# Problem 4
def retrieve_all_jobs():
    keylist = []
    for key in rd.keys():
        keylist.append(key)
    return keylist

# Other problems in hw3.py

if __name__ == '__main__':
    main()


