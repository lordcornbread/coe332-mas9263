#!/usr/bin/python3.6
import sys
sys.path.append('/home/ubuntu/Homework/hw1')
from flask import Flask, jsonify, request, abort
from hw1 import read_from_csv, read_on_timespan, read_with_limit_offset

app = Flask(__name__)

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

def timespan(START, END):
    if START:
        if START.isdigit():
            START = int(START)
        else:
            msg = 'Please enter a valid whole number value for the start year'
            raise InvalidUsage(msg, 400)
    if END:
        if END.isdigit():
            END = int(END)
        else:
            msg = 'Please enter a valid whole number value for the end year'
            raise InvalidUsage(msg, 400)
    if START and END:
        return jsonify(read_on_timespan(start=START, end=END))
    elif START:
        return jsonify(read_on_timespan(start=START))
    elif END:
        return jsonify(read_on_timespan(end=END))

def limit_offset(LIMIT, OFFSET):
    if LIMIT:
        if LIMIT.isdigit():
            LIMIT = int(LIMIT)
        else:
            msg = 'Please enter a valid whole number value for the limit'
            raise InvalidUsage(msg, 400)
    if OFFSET:
        if OFFSET.isdigit():
            OFFSET = int(OFFSET)
        else:
            msg = 'Please enter a valid whole number value for the offset'
            raise InvalidUsage(msg, 400)
    if LIMIT and OFFSET:
        return jsonify(read_with_limit_offset(limit=LIMIT, offset=OFFSET))
    elif LIMIT:
        return jsonify(read_with_limit_offset(limit=LIMIT))
    elif OFFSET:
        return jsonify(read_with_limit_offset(offset=OFFSET))


def get_all():
    return jsonify(read_from_csv())

@app.route('/sunspots', methods=['GET'])
def get_data():
    start   = request.args.get('start')
    end     = request.args.get('end')
    limit   = request.args.get('limit')
    offset  = request.args.get('offset')

    if start or end:
        if limit or offset:
            msg = 'Please restrict your parameter entries to start/end OR limit/offset, not both.'
            raise InvalidUsage(msg, 400)
        return timespan(start, end)
    elif limit or offset:
        return limit_offset(limit, offset)
    else:
        return get_all()

@app.route('/sunspots/<id>', methods=['GET'])
def get_datapoint(id):
    if id.isdigit():
        id = int(id)
    else:
        msg = 'Please enter a valid whole number ID'
        raise InvalidUsage(msg, 400)
    result = read_with_limit_offset(limit=1, offset=id)
    if result:
        return jsonify(result[0])
    else:
        msg = 'ID must be within the range [0, 99]'
        raise InvalidUsage(msg, 400)

@app.route('/sunspots/year/<value>', methods=['GET'])
def get_year(value):
    if value.isdigit():
        year = int(value)
    else:
        msg = 'Please enter a valid whole number year'
        raise InvalidUsage(msg, 400)
    result = read_on_timespan(start=year, end=year+1)
    if result:
        return jsonify(result[0])
    else:
        msg = 'Year must be within the range [1770, 1869]'
        raise InvalidUsage(msg, 400)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
