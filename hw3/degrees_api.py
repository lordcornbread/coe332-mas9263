from flask import Flask, jsonify, request
#import json

# in python interpreter, import requests, then do requests.get('http:localhostL5000/degrees?limit=3').json()

app = Flask(__name__)

def get_data():

    return [{'id': 0, 'year': 1990, 'degrees': 5818},
    {'id': 1, 'year': 1991, 'degrees': 5725},
    {'id': 2, 'year': 1992, 'degrees': 6005},
    {'id': 3, 'year': 1993, 'degrees': 6123},
    {'id': 4, 'year': 1994, 'degrees': 6096},
    ]

@app.route('/degrees', methods=['GET'])
def get_degrees():
    limit = request.args.get('limit') # always a string
    #data = json.dumps(get_data())
    data = jsonify(get_data())
    try:
        lim = int(limit)
    except:
        limit = len(data)
    return data[0:limit]

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
