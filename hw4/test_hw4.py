import requests

# Global variables

id      = 'id'
time    = 'year'
value   = 'sunspots'

BASE_URL = 'http://localhost:5000/sunspots'

START_YEAR = 1770
END_YEAR = 1869
TOTAL_LINES = 100

START_TEST = 1780
END_TEST = 1800
LIMIT_TEST = 10
OFFSET_TEST = 40

INV_PARAM = 'abc'
INV_START = '1880'

YEAR_TEST = 1800
ID_TEST = 50

FAIL = 400
PASS = 200

#================= 1 ==================

# a.
def test_root_status_code():
    assert requests.get(BASE_URL).status_code == PASS
    
# b.
def test_root_json():
    assert requests.get(BASE_URL).json()

# c.
def test_root_json_lists():
    assert type(requests.get(BASE_URL).json()) == list

# d.
def test_root_dicts():
    rsp = requests.get(BASE_URL)
    assert [type(i) == dict for i in rsp.json()]

# e.
def test_root_dict_keys():
    rsp = requests.get(BASE_URL)
    assert [len(i.keys()) == 3 for i in rsp.json()]

# f.
def test_root_dict_value_types():
    rsp = requests.get(BASE_URL)
    assert [type(i[id]) == int for i in rsp.json()]
    assert [type(i[time]) == int for i in rsp.json()]
    assert [type(i[value]) == float for i in rsp.json()]

# g.
def test_root_num_dicts():
    assert len(requests.get(BASE_URL).json()) == TOTAL_LINES

# h.
def test_root_values():
    rsp = requests.get(BASE_URL)
    assert rsp.json()[0][id] == 0
    assert rsp.json()[0][time] == START_YEAR
    assert rsp.json()[0][value] == 101
    assert rsp.json()[-1][id] == TOTAL_LINES - 1
    assert rsp.json()[-1][time] == END_YEAR
    assert rsp.json()[-1][value] == 74

#====================== 2 ====================

# a.
def test_valid_query_status_code_i():
    rsp_i   = requests.get(BASE_URL + '?start=' + str(START_TEST) + '&end=' + str(END_TEST))
    assert rsp_i.status_code == PASS
def test_valid_query_status_code_ii():
    rsp_ii  = requests.get(BASE_URL + '?start=' + str(START_TEST))
    assert rsp_ii.status_code == PASS
def test_valid_query_status_code_iii():
    rsp_iii = requests.get(BASE_URL + '?end=' + str(END_TEST))
    assert rsp_iii.status_code == PASS
def test_valid_query_status_code_iv():
    rsp_iv  = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST) + '&offset=' + str(OFFSET_TEST))
    assert rsp_iv.status_code == PASS
def test_valid_query_status_code_v():
    rsp_v   = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST))
    assert rsp_v.status_code == PASS
def test_valid_query_status_code_vi():
    rsp_vi  = requests.get(BASE_URL + '?offset=' + str(OFFSET_TEST))
    assert rsp_vi.status_code == PASS

# b.
def test_valid_query_json_i():
    rsp_i   = requests.get(BASE_URL + '?start=' + str(START_TEST) + '&end=' + str(END_TEST))
    assert rsp_i.json()
def test_valid_query_json_ii():
    rsp_ii  = requests.get(BASE_URL + '?start=' + str(START_TEST))
    assert rsp_ii.json()
def test_valid_query_json_iii():
    rsp_iii = requests.get(BASE_URL + '?end=' + str(END_TEST))
    assert rsp_iii.json()
def test_valid_query_json_iv():
    rsp_iv  = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST) + '&offset=' + str(OFFSET_TEST))
    assert rsp_iv.json()
def test_valid_query_json_v():
    rsp_v   = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST))
    assert rsp_v.json()
def test_valid_query_json_vi():
    rsp_vi   = requests.get(BASE_URL + '?offset=' + str(OFFSET_TEST))
    assert rsp_vi.json()

# c.
def test_valid_query_list_i():
    rsp_i   = requests.get(BASE_URL + '?start=' + str(START_TEST) + '&end=' + str(END_TEST))
    assert type(rsp_i.json()) == list
def test_valid_query_list_ii():
    rsp_ii  = requests.get(BASE_URL + '?start=' + str(START_TEST))
    assert type(rsp_ii.json()) == list
def test_valid_query_list_iii():
    rsp_iii = requests.get(BASE_URL + '?end=' + str(END_TEST))
    assert type(rsp_iii.json()) == list
def test_valid_query_list_iv():
    rsp_iv  = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST) + '&offset=' + str(OFFSET_TEST))
    assert type(rsp_iv.json()) == list
def test_valid_query_list_v():
    rsp_v   = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST))
    assert type(rsp_v.json()) == list
def test_valid_query_list_vi():
    rsp_vi   = requests.get(BASE_URL + '?offset=' + str(OFFSET_TEST))
    assert type(rsp_vi.json()) == list

# d.
def test_valid_query_objects_i():
    rsp_i   = requests.get(BASE_URL + '?start=' + str(START_TEST) + '&end=' + str(END_TEST))
    assert len(rsp_i.json()) == END_TEST - START_TEST + 1
def test_valid_query_objects_ii():
    rsp_ii  = requests.get(BASE_URL + '?start=' + str(START_TEST))
    assert len(rsp_ii.json()) == END_YEAR - START_TEST + 1
def test_valid_query_objects_iii():
    rsp_iii = requests.get(BASE_URL + '?end=' + str(END_TEST))
    assert len(rsp_iii.json()) == END_TEST - START_YEAR + 1
def test_valid_query_objects_iv():
    rsp_iv  = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST) + '&offset=' + str(OFFSET_TEST))
    assert len(rsp_iv.json()) == LIMIT_TEST
def test_valid_query_objects_v():
    rsp_v   = requests.get(BASE_URL + '?limit=' + str(LIMIT_TEST))
    assert len(rsp_v.json()) == LIMIT_TEST
def test_valid_query_objects_vi():
    rsp_vi  = requests.get(BASE_URL + '?offset=' + str(OFFSET_TEST))
    assert len(rsp_vi.json()) == TOTAL_LINES - OFFSET_TEST

#====================== 3 ====================

# a.
def test_invalid_query_status_code_i():
    rsp = requests.get(BASE_URL + '?start=' + INV_PARAM)
    assert rsp.status_code == FAIL
def test_invalid_query_status_code_ii():
    rsp = requests.get(BASE_URL + '?end=' + INV_PARAM)
    assert rsp.status_code == FAIL
def test_invalid_query_status_code_iii():
    rsp = requests.get(BASE_URL + '?limit=' + INV_PARAM)
    assert rsp.status_code == FAIL
def test_invalid_query_status_code_iv():
    rsp = requests.get(BASE_URL + '?offset=' + INV_PARAM)
    assert rsp.status_code == FAIL
def test_invalid_query_status_code_v():
    rsp = requests.get(BASE_URL + '?start=' + INV_START + '&offset=' + str(OFFSET_TEST))
    assert rsp.status_code == FAIL

# b.
def test_invalid_query_json_i():
    rsp = requests.get(BASE_URL + '?start=' + INV_PARAM)
    assert rsp.json()
def test_invalid_query_json_ii():
    rsp = requests.get(BASE_URL + '?end=' + INV_PARAM)
    assert rsp.json()
def test_invalid_query_json_iii():
    rsp = requests.get(BASE_URL + '?limit=' + INV_PARAM)
    assert rsp.json()
def test_invalid_query_json_iv():
    rsp = requests.get(BASE_URL + '?offset=' + INV_PARAM)
    assert rsp.json()
def test_invalid_query_json_v():
    rsp = requests.get(BASE_URL + '?start=' + INV_START + '&offset=' + str(OFFSET_TEST))
    assert rsp.json()

# c.
def test_invalid_query_msg_i():
    rsp = requests.get(BASE_URL + '?start=' + INV_PARAM)
    assert rsp.json()['message']
def test_invalid_query_msg_ii():
    rsp = requests.get(BASE_URL + '?end=' + INV_PARAM)
    assert rsp.json()['message']
def test_invalid_query_msg_iii():
    rsp = requests.get(BASE_URL + '?limit=' + INV_PARAM)
    assert rsp.json()['message']
def test_invalid_query_msg_iv():
    rsp = requests.get(BASE_URL + '?offset=' + INV_PARAM)
    assert rsp.json()['message']
def test_invalid_query_msg_v():
    rsp = requests.get(BASE_URL + '?start=' + INV_START + '&offset=' + str(OFFSET_TEST))
    assert rsp.json()['message']

#====================== 4 ====================

# a.
def test_id_status_code():
    rsp = requests.get(BASE_URL + '/' + str(ID_TEST))
    assert rsp.status_code == PASS

# b.
def test_id_json():
    rsp = requests.get(BASE_URL + '/' + str(ID_TEST))
    assert rsp.json()

# c.
def test_id_dict():
    rsp = requests.get(BASE_URL + '/' + str(ID_TEST))
    assert type(rsp.json()) == dict

# d.
def test_id_num_keys():
    rsp = requests.get(BASE_URL + '/' + str(ID_TEST))
    assert len(rsp.json().keys()) == 3

# e.
def test_id_id():
    rsp = requests.get(BASE_URL + '/' + str(ID_TEST))
    assert rsp.json()[id] == ID_TEST

#====================== 5 =====================

# a.
def test_time_status_code():
    rsp = requests.get(BASE_URL + '/year/' + str(YEAR_TEST))
    assert rsp.status_code == PASS

# b.
def test_time_json():
    rsp = requests.get(BASE_URL + '/year/' + str(YEAR_TEST))
    assert rsp.json()

# c.
def test_time_dict():
    rsp = requests.get(BASE_URL + '/year/' + str(YEAR_TEST))
    assert type(rsp.json()) == dict

# d.
def test_time_num_keys():
    rsp = requests.get(BASE_URL + '/year/' + str(YEAR_TEST))
    assert len(rsp.json().keys()) == 3

# e.
def test_time_year():
    rsp = requests.get(BASE_URL + '/year/' + str(YEAR_TEST))
    assert rsp.json()[time] == YEAR_TEST
