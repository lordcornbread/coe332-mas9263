#!/usr/bin/python3.6
import pprint

filename = 'sunspots.csv'
value = 'sunspots'
time = 'year'
id = 'id'

#============================ 1 ==============================
def read_from_csv():
    lines = []
    with open(filename,'r') as f:
        for line in f:
            line_dict = {}
            [line_dict[id], line_dict[time], line_dict[value]] = [i.strip() for i in line.split(',')]
            line_dict[id] = int(line_dict[id])
            line_dict[time] = int(line_dict[time])
            line_dict[value] = int(line_dict[value])
            lines.append(line_dict)
    return lines

#============================ 2 ==============================
def read_on_timespan(start=1770, end=1869):
    lines = []
    if start < 1770 or end > 1869:
        return None
    for line_dict in read_from_csv():
        if line_dict[time] in range(start, end+1):
            lines.append(line_dict)
    return lines

#============================ 3 ==============================
def read_with_limit_offset(limit=100, offset=0):
    if offset < 0 or limit > 100:
        return None
    lines = read_from_csv()[offset:limit+offset]
    return lines

#============================ 4 ==============================
def main():
    print('Hi! What query would you like to make?')
    print('Please enter one letter.\n')
    print('a. Whole dataset')
    print('b. Search by start and end year (1770-1869)')
    print('c. Search by limit and offset (100 items total)\n')

    while(1):
        choice = input()
        print('\n')

        if choice=='a' or choice=='A':
            lines = read_from_csv()
            pprint.pprint(lines)
            break
        elif choice=='b' or choice=='B':
            while(1):
                start = input('Start year: ')
                if start.isdigit():
                    start = int(start)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            while(1):
                end = input('End year: ')
                if end.isdigit():
                    end = int(end)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            lines = read_on_timespan(start, end)
            pprint.pprint(lines)
            break
        elif choice=='c' or choice=='C':
            while(1):
                limit = input('Limit: ')
                if limit.isdigit():
                    limit = int(limit)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            while(1):
                offset = input('Offset: ')
                if offset.isdigit():
                    offset = int(offset)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            lines = read_with_limit_offset(limit, offset)
            pprint.pprint(lines)
            break
        elif choice=='q' or choice=='Q':
            break
        else:
            print('That\'s not a valid character. Please enter a, b, or c, or q to quit.')

if __name__ == '__main__':
    main()
