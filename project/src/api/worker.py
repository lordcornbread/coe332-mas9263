import time

from jobs import q, IN_PROGRESS, COMPLETE_STATUS

@q.worker # change this
def execute(job):
    # update job status
    job.update_job_status(IN_PROGRESS)

    # generate a graph from the data
    job.execute_job()

    job.update_job_status(COMPLETE_STATUS)

execute()
