import uuid
import redis
import json
import os
import time
from hotqueue import HotQueue
import numpy as np
import matplotlib.pyplot as plt

from data import read_from_csv, read_on_timespan, read_with_limit_offset

###################################
REDIS_IP = os.environ.get('REDIS_IP', '172.17.0.1')
try:
    REDIS_PORT = int(os.environ.get('REDIS_PORT'))
except:
    REDIS_PORT = 6379

# Redis DBs
DATA_DB = 0
QUEUE_DB = 1

rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=DATA_DB)#, decode_responses=True)
q = HotQueue('queue', host=REDIS_IP, port=REDIS_PORT, db=QUEUE_DB)

# Job Status
SUBMITTED_STATUS = 'submitted'
IN_PROGRESS = 'in_progress'
COMPLETE_STATUS = 'complete'

# Macro to store all jobs created
JOB_LIST = []
JOBS_DICT = {}

# Job class, containing all job-related functions
# 
# Can be specified exactly, or given any of the listed parameters
class Job(object):
    # instantiate job
    def __init__(self, jid='', status='', start=0, end=0, limit=0, offset=0, plot=0):
        self.job_dict = {}
        if jid:
            self.jid = jid
            self.job_dict['jid'] = jid
        else:
            self.jid = self._generate_jid()
            self.job_dict['jid'] = self.jid
        if status:
            self.status = status
            self.job_dict['status'] = status
        if start:
            self.start = start
            self.job_dict['start'] = start
        if end:
            self.end = end
            self.job_dict['end'] = end
        if limit:
            self.limit = limit
            self.job_dict['limit'] = limit
        if offset:
            self.offset = offset
            self.job_dict['offset'] = offset
        if plot:
            self.plot = plot
            self.job_dict['plot'] = plot
        t = time.ctime()
        self.create_time = t
        self.job_dict['create_time'] = self.create_time
        self.last_update_time = t
        self.job_dict['last_update_time'] = self.last_update_time

        JOB_LIST.append(self.job_dict)
        JOBS_DICT[self.jid] = self

    def _generate_jid(self):
        return str(uuid.uuid4())

    def _generate_job_key(self, jid):
        return 'job.{}'.format(jid)

    def _get_job_dict(self):
        return self.job_dict

    # Public methods
    def get_jid(self):
        return self.jid

    def store_job(self):
        rd.set(self.jid, json.dumps(self.job_dict))
        #rd.hmset(self.jid, self.job_dict)
        return self.job_dict

    def add_job(self):#, jid, status, start, end):
        #rd.set(self._generate_job_key(jid), self.job_dict)
        self.queue_job()
        print('Job has been queued.')
        return

    def update_job_status(self, status):
        self.status = status
        self.job_dict['status'] = status
        t = time.ctime()
        self.last_update_time = t
        self.job_dict['last_update_time'] = self.last_update_time
        self.store_job()
        for job in JOB_LIST:
            if job['jid'] == self.jid:
                job['status'] = self.status
                job['last_update_time'] = self.last_update_time
        JOBS_DICT[self.jid] = self
        return

    def delete(self):
        temp_list = JOB_LIST
        for job in JOB_LIST:
            if not job['jid'] == self.uuid:
                temp_list.append(job)
        JOB_LIST = temp_list
        del JOBS_DICT[self.jid]
        rd.delete(self._generate_job_key(self.jid))
        return

    # Queue
    def queue_job(self):
        q.put(self)
        return

    def finalize_job(self, file_path):
        self.status = COMPLETE_STATUS
        self.plot = open(file_path, 'rb').read()
        rd.hmset(self.jid + 'plot', {'plot': self.plot})
        #rd.hmset(self.jid + 'dict', dict(self))
        #self.job_dict['plot'] = self.plot
        for job in JOB_LIST:
            if job['jid'] == self.jid:
                job['plot'] = self.plot
            if job['jid'] == self.jid:
                job['status'] = self.status
        JOBS_DICT[self.jid] = self
        rd.set(self.jid, json.dumps(self._get_job_dict()))
        #rd.hmset(self.jid, self._get_job_dict())
        return

    def get_job_plot(self):
        if not self.status == COMPLETE_STATUS:
            return True, 'job not complete'
        #return False, json.loads(rd.get(self.jid))['plot']
        return False, self.plot

    # Execute worker
    def execute_job(self):
        [start, end, limit, offset] = get_params(self.job_dict)
        points = get_data(start, end, limit, offset)
        years = [int(p['year']) for p in points]
        sunspots = [int(p['sunspots']) for p in points]
        mean = str(np.mean(sunspots))
        rd.set(self.jid + 'mean', mean)
        plt.scatter(years, sunspots)
        plt.xlabel('year')
        plt.ylabel('number of sunspots')
        plt.title('Number of sunspots per year')
        tmp_file = '/tmp/{}.png'.format(self.jid)
        plt.savefig(tmp_file, dpi=150)
        self.finalize_job(tmp_file)
        return

# descriptions
def retrieve_job(uuid):
    if rd.get(uuid):
        return json.loads(rd.get(uuid))
    else:
        return 0

# descriptions
def retrieve_all_jobs():
    return JOB_LIST
    #joblist = []
    #for key in rd.keys():
        #jid, status, start, end = rd.hmget(key, 'id', 'status', 'start', 'end') # 'plot'
        #job_dict = {'id': jid.decode('utf-8'),
        #    'status': status.decode('utf-8'),
        #    'start': start.decode('utf-8'),
        #    'end': end.decode('utf-8'),
        #    'plot': plot.decode('utf-8')
        #};
    #    joblist.append(job_dict)
    #return joblist

def get_data(start, end, limit, offset):
    if start or end:
        data = read_on_timespan(start, end)
    elif limit or offset:
        data = read_with_limit_offset(limit, offset)
    else:
        data = read_from_csv()
    return data

def get_params(job):
    start = None
    end = None
    limit = None
    offset = None
    for key in job.keys():
        if key == 'start':
            start = job[key]
        if key == 'end':
            end = job[key]
        if key == 'limit':
            limit = job[key]
        if key == 'offset':
            offset = job[key]
    return [start, end, limit, offset]


def get_job_plot(jid):
    plot = rd.hmget(jid + 'plot', 'plot')
    return plot

def get_job_mean(jid):
    mean = rd.get(jid + 'mean')
    return float(mean)
