#!/usr/bin/python3.6
import io
import sys
import json
from flask import Flask, jsonify, request, abort, send_file

from data import read_from_csv, read_on_timespan, read_with_limit_offset, add_data_point
from jobs import Job, retrieve_job, retrieve_all_jobs, get_job_plot, get_job_mean
#from worker import get_job_object

app = Flask(__name__)


# Error handler
class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

def timespan(START, END):
    if START:
        if START.isdigit():
            START = int(START)
        else:
            msg = 'Please enter a valid whole number value for the start year'
            raise InvalidUsage(msg, 400)
    if END:
        if END.isdigit():
            END = int(END)
        else:
            msg = 'Please enter a valid whole number value for the end year'
            raise InvalidUsage(msg, 400)
    if START and END:
        if START > 1769 and END < 1871:
            return jsonify(read_on_timespan(start=START, end=END))
        else:
            msg = 'years must be integers between 1770 and 1869'
            raise InvalidUsage(msg, 400)
    elif START:
        if START > 1769:
            return jsonify(read_on_timespan(start=START))
        else:
            msg = 'years must be integers between 1770 and 1869'
            raise InvalidUsage(msg, 400)
    elif END:
        if END < 1871:
            return jsonify(read_on_timespan(end=END))
        else:
            msg = 'years must be integers between 1770 and 1869'
            raise InvalidUsage(msg, 400)

def limit_offset(LIMIT, OFFSET):
    if LIMIT:
        if LIMIT.isdigit():
            LIMIT = int(LIMIT)
        else:
            msg = 'Please enter a valid whole number value for the limit'
            raise InvalidUsage(msg, 400)
    if OFFSET:
        if OFFSET.isdigit():
            OFFSET = int(OFFSET)
        else:
            msg = 'Please enter a valid whole number value for the offset'
            raise InvalidUsage(msg, 400)
    if LIMIT and OFFSET:
        return jsonify(read_with_limit_offset(limit=LIMIT, offset=OFFSET))
    elif LIMIT:
        return jsonify(read_with_limit_offset(limit=LIMIT))
    elif OFFSET:
        return jsonify(read_with_limit_offset(offset=OFFSET))


def get_all():
    return jsonify([x for x in read_from_csv()])

@app.route('/sunspots', methods=['GET'])
def get_data():
    start   = request.args.get('start')
    end     = request.args.get('end')
    limit   = request.args.get('limit')
    offset  = request.args.get('offset')

    if start or end:
        if limit or offset:
            msg = 'Please restrict your parameter entries to start/end OR limit/offset, not both.'
            raise InvalidUsage(msg, 400)
        return timespan(start, end)
    elif limit or offset:
        return limit_offset(limit, offset)
    else:
        return get_all()

@app.route('/sunspots/<ID>', methods=['POST', 'GET'])
def get_datapoint(ID):
    if request.method == 'POST':
        try:
            params = request.get_json(force=True)
        except Exception as e:
            return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
        if params.get('year'):
            try:
                YEAR = int(params.get('year'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'year parameter must be an integer.'})
        else:
            YEAR = False
        if params.get('sunspots'):
            try:
                sunspots = int(params.get('sunspots'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'sunspots parameter must be an integer.'})

        if sunspots:
            if YEAR:
                add_data_point(sunspots, id=ID, year=YEAR) ###################################
            else:
                add_data_point(sunspots, id=ID)
            return jsonify({'status': 'success', 'message': 'submitted'})
        else:
            return jsonify({'status': 'Error', 'message': 'sunspots parameter not entered'})
    elif request.method == 'GET':
        try:
            ID = int(ID)
        except:
            msg = 'Please enter a valid whole number ID'
            raise InvalidUsage(msg, 400)
        result = read_with_limit_offset(limit=1, offset=ID)
        if result:
            return jsonify(result[0])
        else:
            msg = 'ID must be within the range [0, 99]'
            raise InvalidUsage(msg, 400)

@app.route('/sunspots/year/<YEAR>', methods=['GET', 'POST'])
def get_year(YEAR):
    if request.method == 'POST':
        try:
            params = request.get_json(force=True)
        except Exception as e:
            return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
        if params.get('id'):
            try:
                ID = int(params.get('id'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'id parameter must be an integer.'})
        else:
            ID = False
        if params.get('sunspots'):
            try:
                sunspots = int(params.get('sunspots'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'sunspots parameter must be an integer.'})

        if sunspots:
            if ID:
                add_data_point(sunspots, id=ID, year=YEAR) ###################################
            else:
                add_data_point(sunspots, year=YEAR)
            return jsonify({'status': 'success', 'message': 'submitted'})
        else:
            return jsonify({'status': 'Error', 'message': 'sunspots parameter not entered'})
    elif request.method == 'GET':
        if YEAR.isdigit():
            YEAR = int(YEAR)
        else:
            msg = 'Please enter a valid whole number year'
            raise InvalidUsage(msg, 400)
        result = read_on_timespan(start=YEAR, end=YEAR)
        if result:
            return jsonify(result[0])
        else:
            msg = 'Year must be within the range [1770, 1869]'
            raise InvalidUsage(msg, 400)

@app.route('/jobs', methods=['POST', 'GET'])
def something():
    if request.method == 'POST':
        try:
            job = request.get_json(force=True)
        except Exception as e:
            return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
        start = job.get('start')
        if job.get('start'):
            try:
                job['start'] = int(job.get('start'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'start parameter must be an integer.'})
        if job.get('end'):
            try:
                job['end'] = int(job.get('end'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'end parameter must be an integer.'})
        if job.get('limit'):
            try:
                job['limit'] = int(job.get('limit'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'limit parameter must be an integer.'})
        if job.get('offset'):
            try:
                job['offset'] = int(job.get('offset'))
            except:
                return True, json.dumps({'status': 'Error', 'message': 'offset parameter must be an integer.'})
        
        message = {}
        if job.get('status'):
            STATUS = job.get('status')
        else:
            STATUS = None
        if job.get('start'):
            if job.get('start') < 1870:
                START = job.get('start')
                #message['start'] = START
                STATUS = 'submitted'
                message['status'] = STATUS
            else:
                START = None
                STATUS = 'Error'
                message['status'] = STATUS
                message['message'] = 'start year must be greater than 1770'
        else:
            START = None
        if job.get('end'):
            if job.get('end') < 1870:
                END = job.get('end')
                #message['end'] = END
                STATUS = 'submitted'
                message['status'] = STATUS
            else:
                END = None
                STATUS = 'Error'
                message['status'] = STATUS
                message['message'] = 'end year must be less than 1869'
        if job.get('start') and job.get('end'):
            if START > END:
                START = None
                END = None
                STATUS = 'Error'
                message['status'] = STATUS
        else:
            END = None
        if job.get('limit'):
            if job.get('limit') < 100:
                LIMIT = job.get('limit')
                #message['limit'] = LIMIT
                STATUS = 'submitted'
                message['status'] = STATUS
            else:
                LIMIT = None
                STATUS = 'Error'
                message['status'] = STATUS
                message['message'] = 'limit must be less than 100'
        else:
            LIMIT = None
        if job.get('offset'):
            if job.get('offset') < 100:
                OFFSET = job.get('offset')
                #message['offset'] = OFFSET
                STATUS = 'submitted'
                message['status'] = STATUS
            else:
                OFFSET = None
                STATUS = 'Error'
                message['status'] = STATUS
                message['message'] = 'offset must be less than 100'
        else:
            OFFSET = None
        if (job.get('offset') or job.get('limit')) and (job.get('start') or job.get('end')):
            STATUS = 'Error'
            message['status'] = STATUS
            message['message'] = 'you may either enter start/end values or limit/offset values, not both'

        if message['status'] == 'Error':
            return jsonify(message)
        job = Job(status=STATUS, start=START, end=END, limit=LIMIT, offset=OFFSET) 
        jid = job.get_jid()
        result = job.store_job()
        job.add_job()
        message['result'] = result
        #include stuff for limit, offset, start, and end
        return jsonify(message)
    elif request.method == 'GET':
        jobs = retrieve_all_jobs()
        return jsonify(jobs)  # ALL JOBS in database as Json list


# Get information about a single job
@app.route('/jobs/<job_id>', methods=['GET'])
def something3(job_id):
    job = retrieve_job(job_id)
    if job:
        return jsonify(job) # specific job
    else:
        message = {'status': 'Error', 'message': 'There is no job with the specified ID'}
        return jsonify(message)

# Get a plot of the job's data
@app.route('/jobs/<job_id>/plot', methods=['GET'])
def job_plot(job_id):
    #job = get_job_object(str(job_id))
    result = get_job_plot(job_id)
    plot = result[0]#.encode('utf-8') #result[1]
    return send_file(io.BytesIO(plot),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format(job_id))


@app.route('/jobs/<job_id>/mean', methods=['GET'])
def job_mean(job_id):
    result = get_job_mean(job_id)
    return jsonify({'mean': result, 'status': 'success'})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
