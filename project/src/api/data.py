#!/usr/bin/python3.6
import pprint

filename = 'sunspots.csv'
value = 'sunspots'
time = 'year'
id = 'id'

def read_from_csv():
    lines = []
    with open(filename,'r') as f:
        for line in f:
            line_dict = {}
            [line_dict[id], line_dict[time], line_dict[value]] = [i.strip() for i in line.split(',')]
            line_dict[id] = int(line_dict[id])
            line_dict[time] = int(line_dict[time])
            line_dict[value] = int(line_dict[value])
            lines.append(line_dict)
    return lines

def read_on_timespan(start=1770, end=1869):
    lines = []
    if start < 1770 or end > 1869:
        return None
    for line_dict in read_from_csv():
        if line_dict[time] in range(start, end+1):
            lines.append(line_dict)
    return lines

def read_with_limit_offset(limit=100, offset=0):
    if offset < 0 or limit > 100:
        return None
    lines = read_from_csv()[offset:limit+offset]
    return lines

def add_data_point(sunspots, id=0, year=0):
    if not year == 0:
        if id == 0:
            id = read_on_timespan(start=int(year), end=int(year))[0]['id']
    elif not id == 0:
        if year == 0:
            year = read_with_limit_offset(limit=1, offset=int(id))[0]['year']

    lines = []
    with open(filename, 'r') as f:
        for line in f:
            if not line[0] == str(id):
                lines.append(line)
    with open(filename, 'w') as f:
        lines2 = lines
        for line in lines:
            if int(line[0]) < int(id):
                f.write(line)
                lines2 = lines2[1:]
            else:
                break
        f.write(str(id) + ',' + str(year) + ',' + str(sunspots) + '\n')
        for line in lines2:
            f.write(line)


# Main function for running individually; relic from Homework 1
def main():
    print('Hi! What query would you like to make?')
    print('Please enter one letter.\n')
    print('a. Whole dataset')
    print('b. Search by start and end year (1770-1869)')
    print('c. Search by limit and offset (100 items total)\n')

    while(1):
        choice = input()
        print('\n')

        if choice=='a' or choice=='A':
            lines = read_from_csv()
            pprint.pprint(lines)
            break
        elif choice=='b' or choice=='B':
            while(1):
                start = input('Start year: ')
                if start.isdigit():
                    start = int(start)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            while(1):
                end = input('End year: ')
                if end.isdigit():
                    end = int(end)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            lines = read_on_timespan(start, end)
            pprint.pprint(lines)
            break
        elif choice=='c' or choice=='C':
            while(1):
                limit = input('Limit: ')
                if limit.isdigit():
                    limit = int(limit)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            while(1):
                offset = input('Offset: ')
                if offset.isdigit():
                    offset = int(offset)
                    break
                else:
                    print('Invalid year. Please enter a whole number.')
            lines = read_with_limit_offset(limit, offset)
            pprint.pprint(lines)
            break
        elif choice=='q' or choice=='Q':
            break
        else:
            print('That\'s not a valid character. Please enter a, b, or c, or q to quit.')

if __name__ == '__main__':
    main()
