# About
  
COE 332 - Software Engineering and Design
Final Project - REST API frontend to a database of yearly sunspot counts

My DockerHub repo can be found at mattscar/coe332\_project.

by Matthew Scarborough, 12/13/2018
(Please note that this one-person group was not formed intentionally--I think I missed the class where the groups were originally chosen.)
