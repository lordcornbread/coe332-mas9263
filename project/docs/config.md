# About
  
COE 332 - Software Engineering and Design
Final Project - REST API frontend to a database of yearly sunspot counts

My DockerHub repo can be found at mattscar/coe332\_project.

by Matthew Scarborough, 12/13/2018
(Please note that this one-person group was not formed intentionally--I think I missed the class where the groups were originally chosen.)

# Configuration
 - `api.py` holds most of the Flask-specific functions, including the HTML endpoints
 - `jobs.py` contains the Job class, all its helper functions, and the main Redis and queue connections
 - `worker.py` is the continually running worker function
 - `data.py` connects the application to the dataset, by writing to and reading from `sunspots.csv`


# Documentation

### Deployment
 - To run on one VM, simply type `docker-compose up`:
    - Download the project repository from Bitbucket
    - Navigate to the `/project/src` directory
    - Type `docker-compose up` to launch all three containers
    - Note that when running on one VM, it is possible that the Redis container will take a while to launch, and the worker container will be unable to connect to the Redis IP and hence will close. If this occurs, simply rerun the `docker-compose up` command. (I have tried to avoid this problem by inserting an additional `sleep` command in the docker-compose files.
 - To run on two VMs:
    - Download the project repository from Bitbucket onto both machines
    - On the front-end VM, type `ip addr | grep 10.0.2` and copy the 10.0.2.x IP address.
    - Then on the back-end VM, open the `docker-compose-worker.yml` file, and set this IP address to the `REDIS_IP` variable.
    - In the front-end VM, type `docker-compose -f docker-compose-api.yml up`
    - In the back-end VM, type `docker-compose -f docker-compose-worker.yml up`
